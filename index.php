<?php
    include "func/main-functions.php";

// Chargement des pages et de leur fonction associée

    $pages = scandir("inc/pages");

    if (isset($_GET["page"]) && !empty($_GET["page"]) && in_array($_GET["page"].".php",$pages)) {
        $page = $_GET["page"];
    }else{
        $page = "home";
    }

    $pages_functions = scandir("func/");

    if (in_array($page.".func.php",$pages_functions)) {
        include "func/".$page.".func.php";
    }
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bibliothèque</title>

        <!-- Appel des styles CSS -->
        <link rel="stylesheet" href="css/colors.css">
        <link rel="stylesheet" href="css/base.css">
        <link rel="stylesheet" href="css/components.css">
        <!-- Appel des styles de bouton (Google) -->
        <link href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined" rel="stylesheet" />
    </head>
    <body>

        <?php include "inc/header.php"; ?>

        <div class="container">
            
            <?php include "inc/pages/".$page.".php"; ?>

        </div>

        <?php include "inc/footer.php"; ?>
        
        <!-- Bouton de redirection page de connexion / d'enregistrement -->
        <div class="floating-btn-container">
            <a class="btn-floating" href="index.php?page=register"><span class="material-symbols-outlined">fingerprint</span></a>
        </div>

    </body>
</html>

