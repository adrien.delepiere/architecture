<?php

    function get_sum_books() {
        global $db;
        $sql = "SELECT COUNT(id) AS sum FROM tbl_books";
        $res = $db->query($sql);
        $data = $res->fetch_row();
        return $data[0];
    }

    function get_sum_workers() {
        global $db;
        $sql = "SELECT COUNT(id) AS sum FROM tbl_assoc_personnel WHERE role = '1' OR role = '3'";
        $res = $db->query($sql);
        $data = $res->fetch_row();
        return $data[0];
    }

    function get_sum_clients() {
        global $db;
        $sql = "SELECT COUNT(id) AS sum FROM tbl_assoc_personnel WHERE role = '2'";
        $res = $db->query($sql);
        $data = $res->fetch_row();
        return $data[0];
    }