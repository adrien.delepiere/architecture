<?php

function get_users()
{
    global $db;
    $sql = "SELECT prenom, nom FROM tbl_users";
    $res = $db->query($sql);
    $data = $res->fetch_all(MYSQLI_ASSOC);
    return $data;
}