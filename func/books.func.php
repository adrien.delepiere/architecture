<?php

function get_books()
{
    global $db;
    $sql = "SELECT * FROM tbl_books ORDER BY RAND()";
    $res = $db->query($sql);
    $data = $res->fetch_all(MYSQLI_ASSOC);
    return $data;
}