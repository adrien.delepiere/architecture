<?php

function check_book_borrowed($bid)
{
    global $db;
    // On vérifie si le livre est déjà emprunté
    $sql = "SELECT COUNT(id) AS sum FROM tbl_assoc_emprunt WHERE livre = '$bid'";
    $res = $db->query($sql);
    $data = $res->fetch_row();
    if ($data[0] == 0) {
        return false;
    } else {
        return true;
    }
}

function check_client_borrow($uid)
{
    global $db;
    // On vérifie si le client a atteint le max de livres empruntables
    $sql = "SELECT COUNT(id) AS sum FROM tbl_assoc_emprunt WHERE client = '$uid'";
    $res = $db->query($sql);
    $data = $res->fetch_row();
    if ($data[0] < 3) {
        return false;
        
    } else {
        return true;
    }
}

function get_books()
{
    global $db;
    $sql = "SELECT * FROM tbl_books ORDER BY auteur, titre";
    $res = $db->query($sql);
    $data = $res->fetch_all(MYSQLI_ASSOC);
    return $data;
}

function borrow($uid, $bid)
{
    global $db;
    if (check_book_borrowed($bid) && check_client_borrow($uid)) {
        return "Impossible d'emprunter le livre (livre déjà emprunté ou vous possédez déjà 3 livres)";
    } else {
        $sql = "INSERT INTO `tbl_assoc_emprunt` (`client`, `livre`) VALUES ('$uid', '$bid')";
        $db->query($sql);
        return "Livre emprunté !";
    }
}