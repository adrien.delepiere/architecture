# Préparation évaluation

## Base de données

- Ouvrir *Antares* ou *HeidiSQL*
- Créer une nouvelle connexion avec les paramètres suivants

| Paramètre | Valeur |
| --- | --- |
| **DB Host** | `51.68.48.140` |
| **DB User** | `adpe_archi` |
| **DB Pass** | *Censuré* |
| **DB Name** | `adpe_archi` |
| **DB Port** | `3306` |

### Rappel SQL

#### Lire les données d'une table

```
SELECT * FROM nom_de_la_table;
```

#### Ecrire des données dans une table

```
INSERT INTO nom_de_la_table (colonne1, colonne2, colonne3) VALUES ('valeur1', 'valeur2', 'valeur3');
```

#### Supprimer des données dans une table

```
DELETE FROM nom_de_la_table WHERE condition;
```

#### Chercher un texte dans plusieurs colonnes

```
SELECT * FROM nom_de_la_table
WHERE colonne1 LIKE '%texte_recherché%'
   OR colonne2 LIKE '%texte_recherché%'
   OR colonne3 LIKE '%texte_recherché%';
```