<header>
    <a href="index.php?page=home" style="text-decoration: none;">
        <h1 class="white-text center-text">Bibliothèque</h1>
    </a>


    <!-- Affichage des différents lien de redirection de pages -->
    <a href="index.php?page=books"><span class="material-symbols-outlined">menu_book</span></a>
    <a href="index.php?page=books">Livres</a>
    <a href="index.php?page=users"><span class="material-symbols-outlined">person</span></a>
    <a href="index.php?page=users">Utilisateurs</a>
    <a href="index.php?page=contact"><span class="material-symbols-outlined">contact_mail</span></a>
    <a href="index.php?page=contact">Contacts</a>
    <a href="index.php?page=borrow"><span class="material-symbols-outlined">arrow_forward</span></a>
    <a href="index.php?page=borrow">Emprunt</a>
    <a href="index.php?page=borrow"><span class="material-symbols-outlined">logout</span></a>
    <a href="index.php?page=logout">Déconnection</a>

    <p>
        Bienvenue Adrien
        <br/>
        <?= "Nombre de livres empruntés " . get_books_borrowed("11") . "/3"; ?>
    </p>

</header>