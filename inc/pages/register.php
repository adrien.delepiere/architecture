<?php

if (isset ($_SESSION)) {
    echo "Session déjà initialisée";
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_POST["action"] == "login") {
        $login = hash("sha256", $_POST["login"]); // Chiffrement pour comparaison dans la base de données
        $password = hash("sha256", $_POST["password"]); // Chiffrement pour comparaison dans la base de données

        if (login($login, $password) == true) {
            $sql = "SELECT * FROM tbl_users WHERE login = '$login'";
            $res = $db->query($sql);
            $data = $res->fetch_row();
            session_start(); // Connection d'un utilisateur 
            // $_SESSION["uid"] = $data[0]["id"];
        } else {
            echo "Erreur dans la connexion";
        }

    }


    if ($_POST["action"] == "register") {

        $length_num_tel = strlen($_POST["num_tel"]); // Récupération de la longueur du numéro de téléphone
        $prenom = $_POST["prenom"]; // Récupération du prénom du client
        $first_letter = $prenom[0]; // Récupération de la première lettre du prénom du client
        $login = $first_letter . $_POST["nom"]; //Concaténation de la première lettre du prénom du client et le nom du client
        $login = strtolower($login); // Mise en minuscule de la chaine de caractères
        $login = $login . random_int(1000, 9999); // Ajour d'un nombre aléatoire entre 1000 et 9999
        echo $login;
        $login = hash("sha256", $login); // Chiffrement du login

        if ($length_num_tel == 10) {
            $civilite = $_POST["civilite"];
            $prenom = $_POST["prenom"];
            $nom = $_POST["nom"];
            $num_tel = $_POST["num_tel"];
            $adresse = $_POST["adresse"];
            $password = $_POST["password"];
            $password = hash('sha256', $password);
            create_user($civilite, $nom, $prenom, $adresse, $login, $num_tel, $password); // Création d'un nouveau utilisateur
        } else {
            echo "Le numéro de téléphone doit être de 10 chiffres";
        }

    }

}

?>

<h1 style="text-align: center;">Connexion</h1>

<form method="POST" action="#">

    Login
    <input name="login" type="text" placeholder="Login" />
    <input name="password" type="password" placeholder="Mot de passe" />
    <input name="action" type="hidden" value="login" />
    <input name="submit" type="submit" value="Se connecter">
</form>

<hr />

<h1 style="text-align: center;">Inscription</h1>

<div class="white-text">
    <form method="POST" action="#"> <!-- Création d'un formulaire d'envoi de données -->

        <span>Civilité</span>
        <select id="civilite" name="civilite"> <!-- Menu déroulant - Choix de la civilité -->
            <option value="Monsieur">Monsieur</option>
            <option value="Madame">Madame</option>
            <option value="Non binaire">Non binaire</option>
        </select>

        <span>Prénom</span>
        <label for="prenom" class="text-field">
            <input type="text" required name='prenom' placeholder="Prénom">
        </label>

        <span>Nom</span>
        <label for="nom" class="text-field">
            <input type="text" required name='nom' , placeholder="Nom">
        </label>

        <span>Adresse</span>
        <label for="adresse" class="text-field">
            <input type="text" required name='adresse' , placeholder="Adresse">
        </label>

        <span>Numéro Tel</span>
        <label for="num_tel" class="text-field">
            <input type="text" required name='num_tel' , placeholder="Num Tel">
        </label>

        <span>Mot de passe</span>
        <label for="password" class="text-field">
            <input type="text" required name='password' , placeholder="Mot de passe">
        </label>

        <input name="action" type="hidden" value="register" />

        <input type="submit">

    </form>
</div>