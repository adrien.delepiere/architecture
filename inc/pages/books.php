<div class="row">

    <?php foreach (get_books() as $item) { ?>
        <div class="col s4">
            <div class="card">
                <div class="card-image">
                    <?php

                    if ($item["image_url"] == null) {
                        $url = "https://images.unsplash.com/photo-1495446815901-a7297e633e8d?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Ym9va3N8ZW58MHx8MHx8fDA%3D";
                    } else {
                        $url = $item["image_url"];
                    }

                    ?>
                    <img src=<?= $url; ?> alt="<?= $item["titre"]; ?>" />

                    <span class="card-title">
                        <b>
                            <?= $item["titre"]; ?>
                        </b>
                    </span>
                </div>
                <div class="card-content">
                    <p>Nombre de pages :
                        <?= $item["nb_pages"]; ?>
                    <p>
                    <p>Edition :
                        <?= $item["edition"]; ?>
                    <p>
                </div>
            </div>
        </div>
    <?php } ?>

</div>